// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var potongPertama1 = pertama.substring(0, 4);
var potongPertama2 = pertama.substring(12, 18);
var potongKedua = kedua.substring(0, 7);
var potongKedua2 = kedua.substring(8, 18); // Belum dipakai untuk output
var capsKedua2 = potongKedua2.toUpperCase();

console.log(potongPertama1, potongPertama2, potongKedua, capsKedua2);

// Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var intpertama = parseInt(kataPertama);
var intkedua = parseInt(kataKedua);
var intketiga = parseInt(kataKetiga);
var intkeempat = parseInt(kataKeempat);

console.log(intpertama % intketiga * intkedua + intkeempat);

// Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);