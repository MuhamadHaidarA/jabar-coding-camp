// Soal 1
function next_date(tanggalParam, bulanParam, tahunParam){

    var bulanString = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    if(tanggalParam >= 1 && tanggalParam < 31){

        if(tahunParam % 4 == 0){

            if(tanggalParam >= 28 && bulanParam == 2){

                tanggalParam = 1;
                bulanParam++;
            } else {
                tanggalParam++;
            }
        } else {

            tanggalParam++;
        }
        
    } else if(tanggalParam >= 31 && bulanParam == 12){

        tanggalParam = 1;
        bulanParam = 1;
        tahunParam++;
    }
    bulanParam = bulanString[bulanParam - 1];
    return tanggalParam.toString() + " " + bulanParam + " " + tahunParam.toString();
}

var tanggal = 28;
var bulan = 12;
var tahun = 2020;

var tglBerikutnya = next_date(tanggal, bulan, tahun);
console.log(tglBerikutnya);

// Soal 2
function jumlah_kata(kalimat){

    var hitungKata = 0;
    for(var i = 0; i <= kalimat.length; i++){
    
        if(kalimat[i] == " " && kalimat[i + 1] != " " && kalimat[i + 1] != null){

            hitungKata++;
        }
    }

    return hitungKata;
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = " Saya Iqbal";
var kalimat_3 = " Saya Muhammad Iqbal Mubarok ";

var jmlKata = jumlah_kata(kalimat_1);
console.log(jmlKata);