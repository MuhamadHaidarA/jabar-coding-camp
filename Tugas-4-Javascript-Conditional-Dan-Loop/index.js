// Soal 1
var nilai = 80;

if(nilai >= 85){
    console.log("A");
} else if(nilai >= 75 && nilai < 85){
    console.log("B");
} else if(nilai >= 65 && nilai < 75){
    console.log("C");
} else if(nilai >= 55 && nilai < 65){
    console.log("D");
} else if(nilai < 55){
    console.log("E");
}

// Soal 2
var tanggal = 21;
var bulan = 9;
var tahun = 2002;

switch (bulan) {
    case 1: {console.log(tanggal, "Januari", tahun); break;}
    case 2: {console.log(tanggal, "Februari", tahun); break;}
    case 3: {console.log(tanggal, "Maret", tahun); break;}
    case 4: {console.log(tanggal, "April", tahun); break;}
    case 5: {console.log(tanggal, "Mei", tahun); break;}
    case 6: {console.log(tanggal, "Juni", tahun); break;}
    case 7: {console.log(tanggal, "Juli", tahun); break;}
    case 8: {console.log(tanggal, "Agustus", tahun); break;}
    case 9: {console.log(tanggal, "September", tahun); break;}
    case 10: {console.log(tanggal, "Oktober", tahun); break;}
    case 11: {console.log(tanggal, "November", tahun); break;}
    case 12: {console.log(tanggal, "Desember", tahun); break;}

}

// Soal 3
var pagar = "";

console.log("n = 3");
for(var i = 0; i < 3; i++){
    for(var j = 0; j <= i; j++){
        pagar += "#";
    }
    pagar += "\n";
}
console.log(pagar);

pagar2 = "";
console.log("n = 7");
for(var i = 0; i < 7; i++){
    for(var j = 0; j <= i; j++){
        pagar2 += "#";
    }
    pagar2 += "\n";
}
console.log(pagar2);

// Soal 4
var m = 10;
var n = m;
var x = 1;
var y = x;
var samaDengan = "";

for(var a = 1; a <= n; a++){


    if(a % 4 == 0){

        n++;
        y--;
        samaDengan += "===";
        console.log(samaDengan);
    } else {
        switch(x % 4){

            case 1 : {
                console.log(y, " - i love programming");
                break;
            }
            case 2 : {
                console.log(y, " - i love Javascript");
                break;
            }
            case 3 : {
                console.log(y, " - i love VueJS");
                break;
            }
        }
        //console.log(x, " - i love ", x);
    }
    x++;
    y++;
}

