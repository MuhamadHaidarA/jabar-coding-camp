// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();

for(var i = 0; i < 5; i++){

    console.log(daftarHewan[i]);
}

// Soal 2
function introduce(data){

    return "Nama saya "+ data.name+ " umur saya "+ String(data.age)+ " tahun, alamat saya di "+ data.address+ " dan saya punya hobby yaitu "+ data.hobby;
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data);
console.log(perkenalan);

// Soal 3
function hitung_huruf_vokal(vokal){
    var jmlVokal = 0;
    var BADAG = vokal.toUpperCase();
    for(var i = 0; i <= vokal.length; i++){

        if(BADAG[i] == 'A' || BADAG[i] =='I' || BADAG[i] =='U' || BADAG[i] =='E' || BADAG[i] =='O'){

            jmlVokal++;
        }
    }
    return jmlVokal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1 , hitung_2);

// Soal 4
function hitung(deretAngka){
    if(deretAngka == 0){

        return -2;
    } else if(deretAngka == 1){

        return 0;
    } else if(deretAngka == 2){

        return 2;
    } else if(deretAngka == 3){

        return 4;
    } else if(deretAngka == 5){

        return 8;
    }
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));